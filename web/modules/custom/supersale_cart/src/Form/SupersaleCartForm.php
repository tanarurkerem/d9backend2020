<?php

namespace Drupal\supersale_cart\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the supersale cart entity edit forms.
 */
class SupersaleCartForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New supersale cart %label has been created.', $message_arguments));
      $this->logger('supersale_cart')->notice('Created new supersale cart %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The supersale cart %label has been updated.', $message_arguments));
      $this->logger('supersale_cart')->notice('Updated new supersale cart %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.supersale_cart.canonical', ['supersale_cart' => $entity->id()]);
  }

}
