<?php

namespace Drupal\supersale_cart;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a supersale cart entity type.
 */
interface SupersaleCartInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the supersale cart title.
   *
   * @return string
   *   Title of the supersale cart.
   */
  public function getTitle();

  /**
   * Sets the supersale cart title.
   *
   * @param string $title
   *   The supersale cart title.
   *
   * @return \Drupal\supersale_cart\SupersaleCartInterface
   *   The called supersale cart entity.
   */
  public function setTitle($title);

  /**
   * Gets the supersale cart creation timestamp.
   *
   * @return int
   *   Creation timestamp of the supersale cart.
   */
  public function getCreatedTime();

  /**
   * Sets the supersale cart creation timestamp.
   *
   * @param int $timestamp
   *   The supersale cart creation timestamp.
   *
   * @return \Drupal\supersale_cart\SupersaleCartInterface
   *   The called supersale cart entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the supersale cart status.
   *
   * @return bool
   *   TRUE if the supersale cart is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the supersale cart status.
   *
   * @param bool $status
   *   TRUE to enable this supersale cart, FALSE to disable.
   *
   * @return \Drupal\supersale_cart\SupersaleCartInterface
   *   The called supersale cart entity.
   */
  public function setStatus($status);

}
