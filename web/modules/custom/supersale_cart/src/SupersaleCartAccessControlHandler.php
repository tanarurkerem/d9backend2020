<?php

namespace Drupal\supersale_cart;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the supersale cart entity type.
 */
class SupersaleCartAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view supersale cart');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit supersale cart', 'administer supersale cart'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete supersale cart', 'administer supersale cart'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create supersale cart', 'administer supersale cart'], 'OR');
  }

}
