<?php

namespace Drupal\supersale\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\supersale\Form\Buy;

/**
 * Plugin implementation of the 'supersale_buy_default' formatter.
 *
 * @FieldFormatter(
 *   id = "supersale_buy_default",
 *   label = @Translation("Buy"),
 *   field_types = {
 *      "supersale_buy",
 *      "supersale_buywithstock"
 *   }
 * )
 */
class BuyDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();

    foreach ($items as $delta => $item) {

      if ($item->price) {
        $element[$delta]['price'] = [
          '#type' => 'item',
          '#markup' => $item->price . ' Ft.',
        ];
        $form = \Drupal::formBuilder()->getForm(Buy::class, [
          'id' => $entity->id(),
          'EntityType' => $entity->getEntityType()->getOriginalClass(),
          'title' => $entity->title->value,
          'price' => $item->price,
        ]);
        $element[$delta]['buy'] = $form;
      }

    }

    return $element;
  }

}
