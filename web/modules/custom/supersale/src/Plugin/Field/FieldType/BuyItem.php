<?php

namespace Drupal\supersale\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'supersale_buy' field type.
 *
 * @FieldType(
 *   id = "supersale_buy",
 *   label = @Translation("Buy"),
 *   category = @Translation("Supersale"),
 *   default_widget = "supersale_buy",
 *   default_formatter = "supersale_buy_default"
 * )
 */
class BuyItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->price !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['price'] = DataDefinition::create('float')
      ->setLabel(\Drupal::translation()->translate('Price'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $options['price']['NotBlank'] = [];

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ComplexData', $options);
    // @todo Add more constraints here.
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'price' => [
        'type' => 'float',
        'size' => 'normal',
      ],
    ];

    $schema = [
      'columns' => $columns,
      // @DCG Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {

    $scale = rand(1, 5);
    $random_decimal = mt_rand() / mt_getrandmax() * (1000 - 0);
    $values['price'] = floor($random_decimal * pow(10, $scale)) / pow(10, $scale);

    return $values;
  }

}
