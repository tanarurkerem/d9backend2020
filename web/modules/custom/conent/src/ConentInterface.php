<?php

namespace Drupal\conent;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a conent entity type.
 */
interface ConentInterface extends ConfigEntityInterface {

}
