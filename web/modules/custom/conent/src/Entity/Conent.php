<?php

namespace Drupal\conent\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\conent\ConentInterface;

/**
 * Defines the conent entity type.
 *
 * @ConfigEntityType(
 *   id = "conent",
 *   label = @Translation("CONENT"),
 *   label_collection = @Translation("CONENTs"),
 *   label_singular = @Translation("conent"),
 *   label_plural = @Translation("conents"),
 *   label_count = @PluralTranslation(
 *     singular = "@count conent",
 *     plural = "@count conents",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\conent\ConentListBuilder",
 *     "form" = {
 *       "add" = "Drupal\conent\Form\ConentForm",
 *       "edit" = "Drupal\conent\Form\ConentForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "conent",
 *   admin_permission = "administer conent",
 *   links = {
 *     "collection" = "/admin/structure/conent",
 *     "add-form" = "/admin/structure/conent/add",
 *     "edit-form" = "/admin/structure/conent/{conent}",
 *     "delete-form" = "/admin/structure/conent/{conent}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "valami"
 *   }
 * )
 */
class Conent extends ConfigEntityBase implements ConentInterface {

  /**
   * The conent ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The conent label.
   *
   * @var string
   */
  protected $label;

  /**
   * The conent status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The conent description.
   *
   * @var string
   */
  protected $description;

}
